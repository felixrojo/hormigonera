# Hormigonera



## Programa para descargar videos de youtube

Un día, al intentar descargar varias listas de videos de YouTube, me encontré con el problema de que ninguna aplicación lo hacía de la manera que yo deseaba. Hay muchas aplicaciones y páginas web, pero todas tienen algún inconveniente, ya sea que requieren una suscripción o les falta alguna funcionalidad.

Como no estoy dispuesto a utilizar software pirata o pagar por una suscripción, y dado que los programadores somos así, he decidido crear mi propia aplicación. Esta será sencilla, sin complicaciones en la interfaz, solo incluirá las partes indispensables, pero será completamente funcional.

## Funcionamiento

- Añadir enlaces en una lista dar al botón de empezar descargas y ya.

- El enlace se puede asociar a una carpeta para que te descargue la lista o el video dentro de esa carpeta.

- las listas le añada al principio del nombre del archivo un número para poder ordenarlas una vez bajadas.

# Programado por Félix Rojo Trueba.

## Versión Beta 0.5.0