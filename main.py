from tkinter import *
import tkinter as tk
from tkinter import ttk
from tkinter.ttk import Progressbar
from tkinter import filedialog
from pytube import YouTube
from pytube import Playlist
import threading
import os
import sqlite3
import time

VERSION ="1.5.0"
MAX_HILOS = 2

def agregar_texto(texto, color="black"):
    try:
        mensajes.insert(tk.END, texto + '\n', color)
        mensajes.tag_configure(color, foreground=color)
        mensajes.see(tk.END)
    except:
        pass

def elegir_carpeta():
    global carpeta_descarga
    carpeta_descarga = filedialog.askdirectory()
    if carpeta_descarga:
        tk_carpetadestino.config(text=f"Destino: {carpeta_descarga}")
        # mensaje.config(text=f"Carpeta de descarga seleccionada: {carpeta_descarga}")
        agregar_texto(f"Destino: {carpeta_descarga}")

def descargar_video():
    def empezar_descargas():
        id, url, destino = obtener_primer_registro()
        while id != 0:
            if 'playlist' in url.lower():
                estado_casilla = casilla_verificacion.get()
                if estado_casilla:
                    playlist = Playlist(url).title.replace("/", "").replace("\\", "")
                    destino += f'\{playlist}'
                    if not os.path.exists(destino):
                        os.makedirs(destino)
                hilo1 = threading.Thread(target=descargar_lista_de_reproduccion, args=(url,destino))
                hilo1.start()
            else:
                ds = descargas_simultaneas()
                while len(threading.enumerate())>MAX_HILOS + ds:
                    time.sleep(2)
                hilo2 = threading.Thread(target=descargar_video_individual, args=(url, destino))
                hilo2.start()
            try:
                hilo1.join()
                hilo2.join()
            except:
                pass
            borrar_id(id)
            listar_datos()
            id, url, destino = obtener_primer_registro()
        agregar_texto(f"TODOS LOS VIDEOS ESTÁN EN COLA", "blue")
        simultaneas_entry.config(state=tk.NORMAL)
    threading.Thread(target=empezar_descargas).start()
        

def descargar_lista_de_reproduccion(url, destino):
    simultaneas_entry.config(state=tk.DISABLED)
    playlist = Playlist(url)
    total_videos = len(playlist.videos)
    agregar_texto(f"lista de reproducion: {playlist.title} ({len(playlist.videos)} Videos)", "blue")
    for i, video in enumerate(playlist.videos, start=1):
        progreso = ((i) / total_videos) * 100
        ds = descargas_simultaneas()
        while len(threading.enumerate())>MAX_HILOS + ds:
                    time.sleep(2)
        descargar_video_individual(video.embed_url, destino, i, total_videos)
    # mensaje.config(text="¡Descarga de lista de reproducción en la cola")
    listar_datos()
    
def temporales():
    ds = descargas_simultaneas()
    conexion = sqlite3.connect('datos.db')
    cursor = conexion.cursor()
    # Ejecutar la consulta para obtener todas las filas de la tabla
    cursor.execute(f"SELECT * FROM tmp")
    filas = cursor.fetchall()
    cursor.execute(f"DELETE FROM tmp")
    conexion.commit()
    conexion.close()
    for fila in filas:
        borrar_tmp(fila[0])
        descargar_video_individual(fila[1], fila[2], fila[3], fila[4])
    agregar_texto(f"DESCARGANDO TEMPORALES QUE SE QUEDARON EN LA COLA EN EL ULTIMO CIERRE","BLACK")
    # Cerrar la conexión con la base de datos
    
def descargas_simultaneas():
    try:
        ds = int(simultaneas_entry.get())
    except:
        return 1
    if ds <=0 :
        return 1
    else:
        return ds
        
def descargar_video_individual(url, destino, numero=0, total_videos=0):
    ds = descargas_simultaneas()
    def descargar():
        id = url
        agregar_a_tmp(id, url, destino, numero, total_videos)
        intentos = 10  # Número máximo de intentos
        while intentos > 0:
            try:              
                frames[threading.current_thread().name] = Frame(listbox, width=300, height=200, padx=10)
                frames[threading.current_thread().name].pack(fill="both", expand=True)
                titulos[threading.current_thread().name] = Label(frames[threading.current_thread().name], text="")
                titulos[threading.current_thread().name].pack(fill="both", expand=True)
                barras[threading.current_thread().name] = Progressbar(frames[threading.current_thread().name], orient=HORIZONTAL, length=300, mode='determinate')
                barras[threading.current_thread().name].pack(fill="both", expand=True)
                porcentajes[threading.current_thread().name] = Label(frames[threading.current_thread().name], text="0 %")
                porcentajes[threading.current_thread().name].pack(fill="both", expand=True)
                yt = YouTube(url, on_progress_callback=progreso_descarga)
                video = yt.streams.get_highest_resolution()
                titulos[threading.current_thread().name].config(text=f"{video.title} - {int(video.filesize_mb)} Megas")
                if numero == 0:
                    prefijo=""
                else:
                    prefijo =f'{numero} de {total_videos} '
                agregar_texto(f"Descargando {prefijo}: {video.title}")
                video.download(output_path=destino,filename_prefix=prefijo)
                agregar_texto(f"Descargado  {prefijo}: {yt.title}-->Completa", "green")
                # mensaje.config(text="¡Descarga completa!")
                barras[threading.current_thread().name].destroy()
                porcentajes[threading.current_thread().name].destroy()
                titulos[threading.current_thread().name].destroy()
                frames[threading.current_thread().name].destroy()
                # creo archivo y pongo la descripcion
                try:
                    title =yt.title
                    with open(destino+"\descripciones.txt", 'a') as archivo:
                        # Escribe el texto en el archivo
                        archivo.write(f'{yt.title}\n\n{yt.description}\n\n\n')
                except Exception as e:
                    print(f'Error: {e}')
                
                borrar_tmp(id)
                listar_datos()
                
                actualizar_gigas_y_videos(video.filesize_gb,1)
                gigas = 0
                videos = 0
                gigas, videos = leer_configuracion_gigas_videos()
                autor.config(text=f' {videos} Videos descargados, {gigas:.2f} Gigas descargados')
                break  # Sale del bucle si la descarga fue exitosa
            except Exception as e:
                agregar_texto(f"Error en la descarga: {str(e)}", "red")
                agregar_texto(f"Reintentando la descarga en 10 segundos quedan {intentos} intentos", "red")
                
                intentos -= 1  # Reduce el número de intentos restantes
                if intentos == 0:
                    agregar_texto("Se excedió el número máximo de intentos. Ve1rifica tu conexión y vuelve a intentarlo.", "red")
                    borrar_tmp(id)
                if not barras[threading.current_thread().name] in barras:
                    barras[threading.current_thread().name].destroy()
                    porcentajes[threading.current_thread().name].destroy()
                    titulos[threading.current_thread().name].destroy()
                    frames[threading.current_thread().name].destroy()
                time.sleep(10)
    
    threading.Thread(target=descargar).start()

def progreso_descarga(stream, chunk, remaining):
    progreso = (1 - remaining / stream.filesize) * 100
    try:
        barras[threading.current_thread().name]['value'] = progreso
        porcentajes[threading.current_thread().name].config(text="{:.1f}".format(progreso)+" % "+f'{remaining//1024//1024} Megas' )
    except:
        pass
    
def agregar_a_tmp(id, url, destino, inicio=0, fin=0):
    # Conectar a la base de datos
    conexion = sqlite3.connect('datos.db')
    cursor = conexion.cursor()

    # Insertar datos en la tabla sin incluir el campo 'id'
    try:
        cursor.execute("INSERT INTO tmp (id, url, destino, numero, fin) VALUES (?, ?, ?, ?, ?)", (id, url, destino, inicio, fin))
    except:
        agregar_texto(f"Demasiadas descargas simultaneas puede provocar perdida de videos.\nNo se guarda en TMP{url}","red")
    # Confirmar los cambios y cerrar la conexión
    conexion.commit()
    conexion.close()
    agregar_texto(f"TMP: {url} en carpeta {destino}","HotPink")
    url_entry.delete(0, tk.END)
    listar_datos()    
    
def borrar_tmp(id):
     # Conectar a la base de datos
    conexion = sqlite3.connect('datos.db')
    cursor = conexion.cursor()

    # Borrar el registro con el ID especificado
    cursor.execute("DELETE FROM tmp WHERE id=?", (id,))
    agregar_texto(f"TMP: {id} borrado","HotPink")
    # Confirmar los cambios y cerrar la conexión
    conexion.commit()
    conexion.close()
    
def agregar_a_tabla():
    url = url_entry.get()
    if url=="":
        agregar_texto(f"Error, no Tienes ningun enlace.", "red")
        return
    destino = carpeta_descarga
    if carpeta_descarga == "":
        destino = os.path.expanduser("~/Downloads")

    # Conectar a la base de datos
    conexion = sqlite3.connect('datos.db')
    cursor = conexion.cursor()

    # Insertar datos en la tabla sin incluir el campo 'id'
    cursor.execute("INSERT INTO descargas (url, destino) VALUES (?, ?)", (url, destino))

    # Confirmar los cambios y cerrar la conexión
    conexion.commit()
    conexion.close()
    agregar_texto(f"Añadido enlace: {url} en carpeta {destino}","DarkMagenta")
    url_entry.delete(0, tk.END)
    listar_datos()
    
def obtener_id_seleccionado():
    seleccion = treeview.selection()
    if seleccion:
        item = treeview.item(seleccion[0])
        id_seleccionado = item['values'][0]
        borrar_id(id_seleccionado)
        listar_datos()

def listar_datos():
    # Conectar a la base de datos
    conexion = sqlite3.connect('datos.db')
    cursor = conexion.cursor()

    # Ejecutar una consulta para obtener todos los datos de la tabla
    cursor.execute("SELECT * FROM descargas")
    datos = cursor.fetchall()

    # Cerrar la conexión
    conexion.close()

    # Limpiar el Treeview
    for i in treeview.get_children():
        try:
            treeview.delete(i)
        except:
            pass

    # Agregar los datos al Treeview
    for dato in datos:
        treeview.insert("", "end", values=dato)   
    #scroll al final
    try:
        if treeview.get_children():
            treeview.see(treeview.get_children()[-1])
    except:
        pass
    
def borrar_id(id):
 
    # Conectar a la base de datos
    conexion = sqlite3.connect('datos.db')
    cursor = conexion.cursor()

    # Borrar el registro con el ID especificado
    cursor.execute("SELECT url FROM descargas WHERE id=?", (id,))
    resultado = cursor.fetchone()
    cursor.execute("DELETE FROM descargas WHERE id=?", (id,))
    if resultado:
        agregar_texto(f"Enlace: {resultado[0]} borrado","Blue")
    # Confirmar los cambios y cerrar la conexión
    conexion.commit()
    conexion.close()

def obtener_primer_registro():
    # Conectar a la base de datos
    conexion = sqlite3.connect('datos.db')
    cursor = conexion.cursor()

    # Ejecutar una consulta para obtener el primer registro
    cursor.execute("SELECT * FROM descargas LIMIT 1")
    primer_registro = cursor.fetchone()

    # Cerrar la conexión
    conexion.close()

    if primer_registro:
        # Si hay un registro, devolver los valores id, url y destino
        id, url, destino = primer_registro
        return id, url, destino
    else:
        # Si no hay registros, devolver 0
        return 0, None, None
    
def actualizar_gigas_y_videos(gigas, videos):
    # Conectar a la base de datos SQLite
    conexion = sqlite3.connect('datos.db')
    cursor = conexion.cursor()

    # Verificar si el registro con id 1 ya existe
    cursor.execute('SELECT * FROM config WHERE id = 1')
    registro_existente = cursor.fetchone()

    if registro_existente:
        # Si existe, actualizar el registro con id 1
        cursor.execute('''
            UPDATE config
            SET gigas = gigas + ?,
                videos = videos + ?
            WHERE id = 1
        ''', (gigas, videos))
    else:
        # Si no existe, insertar un nuevo registro con id 1
        cursor.execute('''
            INSERT INTO config (id, gigas, videos)
            VALUES (1, ?, ?)
        ''', (gigas, videos))

    # Confirmar los cambios
    conexion.commit()

    # Cerrar la conexión con la base de datos
    conexion.close()
    
def leer_configuracion_gigas_videos():
    # Conexión a la base de datos (asegúrate de tener el archivo de la base de datos en el mismo directorio)
    conexion = sqlite3.connect('datos.db')
    cursor = conexion.cursor()

    try:
        # Ejecutar una consulta para obtener los valores de gigas y videos para el id 1
        cursor.execute('SELECT gigas, videos FROM config WHERE id = 1')
        resultado = cursor.fetchone()

        if resultado:
            gigas, videos = resultado
            return gigas, videos
        else:
            return None

    except sqlite3.Error as e:
        return None

    finally:
        # Cerrar la conexión
        conexion.close()
def mostrar_tooltip_simultaneas(event, texto, x, y):
    tooltip_label_simultaneas.place(x=x, y=y + simultaneas_entry.winfo_height())
    tooltip_label_simultaneas.config(text=texto)

def ocultar_tooltip_simultaneas(event):
    tooltip_label_simultaneas.place_forget()
    
# Configuración de la interfaz gráfica
root = Tk()
root.title(f"Hormigonera {VERSION}")
barras = {}
porcentajes = {}
titulos ={}
frames = {}

frame = Frame(root, padx=10, pady=10)
frame.pack(padx=10, pady=10)

# frame de archivos
frame_archivos = Frame(frame, width=300, height=200, padx=10, pady=10)
frame_archivos.grid(row=0, columnspan=5, sticky="nsew")

# contenidoi de frame archivos
label = Label(frame_archivos, text="Enlace de YouTube:")
label.grid(row=0, column=0,columnspan=3, sticky=W)

url_entry = Entry(frame_archivos, width=80)
url_entry.grid(row=0, column=4)

tk_carpetadestino = Label(frame_archivos, text="", width=80, anchor="w")
tk_carpetadestino.grid(row=1, column=0, columnspan=5, sticky="nsew")

carpeta_button = Button(frame_archivos, text="Elegir Carpeta", command=elegir_carpeta)
carpeta_button.grid(row=1, column=5)

add_enlace = Button(frame_archivos, text="Añadir enlace", command=agregar_a_tabla, anchor="w")
add_enlace.grid(row=2, column=0, columnspan=5, sticky="w", pady=10)

quit_enlace = Button(frame_archivos, text="Quitar enlace", command=obtener_id_seleccionado, anchor="w")
quit_enlace.grid(row=2, column=2, columnspan=5, sticky="w",padx=(100))

casilla_verificacion = tk.BooleanVar()
casilla = tk.Checkbutton(frame_archivos, text="Crear Carpeta para listas de reproducción", variable=casilla_verificacion)
casilla.grid(row=3, columnspan=5, sticky=W)
casilla.bind("<Enter>", lambda event: mostrar_tooltip_simultaneas(event, "Creara una carpeta de nombre numero del id con todos los videos de la lista.", 20, 130))
casilla.bind("<Leave>", ocultar_tooltip_simultaneas)
casilla_verificacion.set(True)

label_simultaneas = Label(frame_archivos, text="Descargas simultaneas:")
label_simultaneas.grid(row=2, column=4, sticky=E)

simultaneas_entry = Entry(frame_archivos, width=3)
simultaneas_entry.insert(0, "10")
simultaneas_entry.grid(row=2, column=5)
simultaneas_entry.bind("<Enter>", lambda event: mostrar_tooltip_simultaneas(event, "Si se ponen muchas descargas simultaneas el programa ralentizará.", 450, 100))
simultaneas_entry.bind("<Leave>", ocultar_tooltip_simultaneas)
tooltip_label_simultaneas = tk.Label(root, text="Tooltip", bg="lightyellow", relief="solid", borderwidth=1)
ds = descargas_simultaneas()

# Crear el Treeview
treeview = ttk.Treeview(frame_archivos, columns=("ID", "URL", "Destino"), show="headings")

# Definir las columnas
treeview.heading("ID", text="ID")
treeview.heading("URL", text="URL")
treeview.heading("Destino", text="Destino")

# Ajustar el ancho de las columnas
treeview.column("ID", width=5)
treeview.column("URL", width=300)
treeview.column("Destino", width=300)

# treeview.grid(row=3, column=0,columnspan=6, sticky=(tk.W, tk.E, tk.N, tk.S))
# Crear una barra de desplazamiento vertical
scrollbar = ttk.Scrollbar(frame_archivos, orient="vertical", command=treeview.yview)
treeview.configure(yscrollcommand=scrollbar.set)

# Colocar el Treeview y la barra de desplazamiento en el frame
treeview.grid(row=4, column=0, columnspan=6, sticky=(tk.W, tk.E, tk.N, tk.S))
scrollbar.grid(row=4, column=6, sticky=(tk.N, tk.S))

# frame de descargas
frame_descargas = Frame(frame, width=200, height=200, padx=10, pady=10)
frame_descargas.grid(row=0,column=6, columnspan=5, rowspan=2, sticky="nsew")

# contenido frame descargas
autor = Label(frame_descargas, text="Programado por: Félix Rojo Trueba", width=80, anchor="w")
autor.grid(row=0, column=0, columnspan=5, sticky="nsew", pady=10)
descargar_button = Button(frame_descargas, text="Empezar Descargas", command=descargar_video)
descargar_button.grid(row=1,column=4)

# Crear el Listbox
listbox = tk.Listbox(frame_descargas, width=200, height=0)
listbox.place(x=0, y=80, relwidth=1)


# frame de informacion
frame_info = Frame(frame, width=300, height=200, padx=10, pady=10)
frame_info.grid(row=1,column=0, columnspan=5, sticky="nsew")

# contenido frame info
mensajes = tk.Text(frame_info, wrap="word", width=85, height=30)
# Agregar una barra de desplazamiento vertical al widget Text
scrollbar_mensajes = ttk.Scrollbar(frame_info, orient="vertical", command=mensajes.yview)
mensajes.configure(yscrollcommand=scrollbar_mensajes.set)

# Colocar el widget Text y la barra de desplazamiento en el frame_info
mensajes.grid(row=0, columnspan=4, sticky="nsew")
scrollbar_mensajes.grid(row=0, column=4, sticky="ns")

mensaje = Label(frame_info, text="Programado por: Félix Rojo Trueba")
mensaje.grid(row=1, columnspan=5, sticky="nsew")
carpeta_descarga = os.path.expanduser("~/Downloads")
tk_carpetadestino.config(text=f"Destino: {carpeta_descarga}")
# mensaje.config(text=f"Carpeta de descarga seleccionada: {carpeta_descarga}")
agregar_texto(f"Destino: {carpeta_descarga}")

# Crear tablas si no existe
conexion = sqlite3.connect('datos.db')
cursor = conexion.cursor()
cursor.execute("CREATE TABLE IF NOT EXISTS descargas (id INTEGER PRIMARY KEY AUTOINCREMENT, url TEXT, destino TEXT)")
cursor.execute("CREATE TABLE IF NOT EXISTS tmp (id TEXT, url TEXT, destino TEXT, numero INTEGER, fin INTEGER)")
cursor.execute('''
    CREATE TABLE IF NOT EXISTS config (
        id INTEGER PRIMARY KEY,
        descargas_simultaneas INTEGER,
        gigas FLOAT,
        videos INTEGER
    )
''')
# si se paro con alguna descarga que la continue
cursor.execute("SELECT COUNT(*) FROM tmp")
cantidad_filas = cursor.fetchone()[0]
if cantidad_filas > 0:
    temporales()
conexion.commit()
conexion.close()
borrar_id(3)
listar_datos()


root.mainloop()
